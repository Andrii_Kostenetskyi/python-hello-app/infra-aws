resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr_vpc
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Environment = var.environment_tag
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
  tags = {
    Environment = var.environment_tag
  }
}

resource "aws_subnet" "subnet_public" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.cidr_subnet
  map_public_ip_on_launch = "true"
  availability_zone       = var.availability_zone
  tags = {
    Environment = var.environment_tag
  }
}

resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Environment = var.environment_tag
  }
}

resource "aws_route_table_association" "rta_subnet_public" {
  subnet_id      = aws_subnet.subnet_public.id
  route_table_id = aws_route_table.rtb_public.id
}

resource "aws_key_pair" "deployer" {
  key_name   = "technical_user"
  public_key = var.ssh_public_key
  tags = {
    Environment = var.environment_tag
  }
}

data "template_file" "user_data" {
  template = file("cloudinit/baseline_config.yaml")
}

resource "aws_instance" "testInstance" {
  count                  = var.instance_count
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.subnet_public.id
  vpc_security_group_ids = ["${aws_security_group.sg_22.id}", "${aws_security_group.sg_8080.id}"]
  user_data              = data.template_file.user_data.rendered
  tags = {
    Environment = var.environment_tag
  }
}

resource "aws_instance" "ProdInstance" {
  count                  = var.instance_count
  ami                    = var.instance_ami
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.subnet_public.id
  vpc_security_group_ids = ["${aws_security_group.sg_22.id}", "${aws_security_group.sg_8080.id}"]
  user_data              = data.template_file.user_data.rendered
  tags = {
    Environment = var.environment_tag
  }
}

