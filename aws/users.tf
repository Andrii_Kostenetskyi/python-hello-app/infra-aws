############
# IAM users
############
module "iam_user1" {
  source = "../modules/iam-user"

  name = "tipoc002"

  create_iam_user_login_profile = false
  create_iam_access_key         = false
}

######################
# IAM policy (sample)
######################
data "aws_iam_policy_document" "s3access" {
  statement {
    actions = [
      "s3:ListBuckets",
    ]

    resources = ["*"]
  }
}