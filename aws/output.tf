output "vpc_id" {
  value = aws_vpc.vpc.id
}
output "public_subnets" {
  value = ["${aws_subnet.subnet_public.id}"]
}
output "public_route_table_ids" {
  value = ["${aws_route_table.rtb_public.id}"]
}
output "public_instance_ip" {
  value = ["${aws_instance.testInstance.*.public_ip}"]
}

output "public_instance_ip_prod" {
  value = ["${aws_instance.ProdInstance.*.public_ip}"]
}

output "caller_identity_account_id" {
  description = "The ID of the AWS account"
  value       = module.iam_account.caller_identity_account_id
}

output "iam_account_password_policy_expire_passwords" {
  description = "Indicates whether passwords in the account expire. Returns true if max_password_age contains a value greater than 0. Returns false if it is 0 or not present."
  value       = module.iam_account.iam_account_password_policy_expire_passwords
}
