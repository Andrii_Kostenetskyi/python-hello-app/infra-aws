resource "null_resource" "ConfigureAnsibleLabelVariable" {
  provisioner "local-exec" {
    command = "echo [${var.dev_host_label}:vars] > ../hosts"
  }
  provisioner "local-exec" {
    command = "echo ansible_ssh_user=${var.ssh_user_name} >> ../hosts"
  }
  provisioner "local-exec" {
    command = "echo [${var.dev_host_label}] >> ../hosts"
  }
}

resource "null_resource" "ProvisionRemoteHostsIpToAnsibleHosts" {
  count = var.instance_count
  connection {
    type = "ssh"
    user = var.ssh_user_name
    host = element(aws_instance.testInstance.*.public_ip, count.index)
  }
  provisioner "local-exec" {
    command = "echo ${element(aws_instance.testInstance.*.public_ip, count.index)} >> ../hosts"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -i ../hosts ../site.yml"
  }
  depends_on = [
    null_resource.ConfigureAnsibleLabelVariable,
  ]
}

resource "null_resource" "ConfigureAnsibleLabelVariableProd" {
  provisioner "local-exec" {
    command = "echo [${var.prod_host_label}:vars] >> ../hosts"
  }
  provisioner "local-exec" {
    command = "echo ansible_ssh_user=${var.ssh_user_name} >> ../hosts"
  }
  provisioner "local-exec" {
    command = "echo [${var.prod_host_label}] >> ../hosts"
  }
  depends_on = [
    null_resource.ProvisionRemoteHostsIpToAnsibleHosts,
  ]

}

resource "null_resource" "ProvisionRemoteHostsIpToAnsibleHostsProd" {
  count = var.instance_count
  connection {
    type = "ssh"
    user = var.ssh_user_name
    host = element(aws_instance.testInstance.*.public_ip, count.index)
  }
  provisioner "local-exec" {
    command = "echo ${element(aws_instance.ProdInstance.*.public_ip, count.index)} >> ../hosts"
  }
  provisioner "local-exec" {
    command = "ansible-playbook -i ../hosts ../site.yml"
  }
  depends_on = [
    null_resource.ConfigureAnsibleLabelVariableProd,
  ]
}