provider "aws" {
  region = var.region
}

##############
# IAM account
##############
module "iam_account" {
  source = "../modules/iam-account"

  account_alias = "hello-app"

  minimum_password_length = 6
  require_numbers         = false
}

#####################################################################################
# IAM group for users with custom access
#####################################################################################
module "iam_group_with_custom_policies" {
  source = "../modules/iam-group-with-policies"

  name = "custom_ecr_access"

  group_users = [
    module.iam_user1.iam_user_name,
  ]

  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser",
  ]

  custom_group_policies = [
    {
      name   = "AllowS3Listing"
      policy = data.aws_iam_policy_document.s3access.json
    },
  ]
}