terraform {
  backend "s3" {
    bucket           = "django-rest-api-poc-tfstate"
    region           = "us-east-1"
    force_path_style = true
    key              = "terraform/aws/django-rest-api.tfstate"
    profile          = "default"
  }
}
