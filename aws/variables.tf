# Variables
variable "region" {
  default = "us-east-1"
}

variable "cidr_vpc" {
  description = "CIDR block for the VPC"
  default     = "10.1.0.0/16"
}

variable "cidr_subnet" {
  description = "CIDR block for the subnet"
  default     = "10.1.0.0/24"
}

variable "availability_zone" {
  description = "availability zone to create subnet"
  default     = "us-east-1b"
}

variable "instance_ami" {
  description = "AMI for aws EC2 instance"
  default     = "ami-0aeeebd8d2ab47354"
}
variable "instance_count" {
  default = "1"
}
variable "instance_type" {
  description = "type for aws EC2 instance"
  default     = "t3.small"

}
variable "environment_tag" {
  description = "Environment tag"
  default     = "POC"
}

variable "ssh_key_path" {
  default = "~/.ssh/aws_keys.pem"
}

variable "ssh_public_key" {
  default = ""
}

variable "ssh_user_name" {
  default = "ec2-user"
}

variable "dev_host_label" {
  default = "dev"
}

variable "prod_host_label" {
  default = "prod"
}
