Table of Contents
-----------------
1. [Description](#description)
2. [Pipeline](#pipeline)
3. [License](#license)
4. [Author](#author-information)

Description
=========

Repo provides terraform code deploying EC2 instance all accompanying resources required to serve the application. 
Repo also illustrates development lifecycle with terraform and ansible that can be used in a team following strict gitops model,
with peers review in MR and manual final deploy trigger once code has been merged to a master.


Pipeline
------------

A multistage gitlab-ci pipeline:
- validate stage:
  - init with testing of providers and setup of environment
  - validate will do syntax check
- Costs validation stage:
  -  [terraform-cost-estimation](https://github.com/antonbabenko/terraform-cost-estimation) tool was used to elaborate a budget/costs test where the monthly budget threshold is given as an input variable. The estimation is done with a simple condition to check if the planned deployment goint to exceed the threshold or not. [terraform.jq](https://github.com/antonbabenko/terraform-cost-estimation/blob/master/terraform.jq) is used  - all the credits for both solutions go to developer [Anton Babenko](https://github.com/antonbabenko)
- build stage:
  - will not only run plan and in-built in terraform code validation, but also is going to save it in .json format and cache it
  - is going to generate native terraform gitlab-ci report presenting it in MR
  - is going to make API call to gitlab server creating a discussions comment attaching pre-formatted plan using gitlab access token - that will ensure a peers during MR review can directly check in UI what has been created/changed/deleted in a plan.
- deploy: 
  - only when the MR is going to be merged to master this job is going to be activated giving a final manual step to press a button to actually deploy the resources (based on gitlab RBAC and master commits ruleset - this can be user with higher privileges).
  - after deployment of all necessary resources, ansible-playbook will be triggered for the ability to configure all ci part for hello-app repository

Pipeline variables
------------
AWS_ACCESS_KEY_ID - your access key for access via the technical account for the deployments of resources on Amazon
AWS_SECRET_ACCESS_KEY - your private key for access via the technical account for the deployments of resources on Amazon
ec2_region - default region 
RUNNER_REGISTRATION_TOKEN - this is a registration token, which must point to hello-app repository on GitLab (for the ability to auto add runners to the main repo of hello-app)
TF_VAR_ssh_public_key - your ssh public key, which will be used for access to newly created EC2 instances


Additional module
------------
- Terraform modules:
  -  [terraform-aws-iam](https://github.com/terraform-aws-modules/terraform-aws-iam) tool was used to manage IAM users/roles. 
     all the credits for both solutions go to developer [Anton Babenko](https://github.com/antonbabenko)


License
-------

GNU GPL license


Author Information
------------------

Andrii KOSTENETSKYI




