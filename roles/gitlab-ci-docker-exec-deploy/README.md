Table of Contents
-----------------
1. [Role Description](#role-description)
2. [Tested on](#tested-on)
3. [Infra Requirements](#infra-requirements)
4. [OS Requirements](#os-requirements)
5. [Ansible Requirements](#ansible-requirements)
6. [Role Variables](#role-variables)
7. [Dependency Roles](#dependency-roles)
8. [Example Playbook](#example-playbook)
9. [License](#license)
10. [Author](#author-information)

Role Description
=========

This role provides a complete orchestration of deployment of gitlab ci-runner as docker executor. Not only the runner is deployed in container, role also builds a service docker image that runner is going to spown during each commit (running ci proccess on this container). Currently only python-runtime is presented as service image, but obviously the sky is a limit - the role is built to accommodate further added service images. 

This readme is focused on low-level functionality of this particular role. In case you have troubles understanding how to use this role (for instance, how and where define variables) as well as to learn more about general Docs-DevOps code-base ansible guidelines, best-practices and development methodics, please visit our main page github readme @ <>

Tested on
------------

|Distribution|Ansible 2.9.6|
|------------|-----------|
|Centos 7.7|yes|


|Distribution| Python|
|------------|-----------|
Centos 7|3.6.8|
Centos 7|2.7|

Infra Requirements
------------

It is presumed that VM is already provisioned and ready with OS. As always make sure from your host machine you can reach gitlab instance via tcp/44 (here it is still gitlab.com).


OS Requirements
------------

Python virtenv deloyed (either with python2 or python3), see more in Dependancy roles. **Do NOT use** SCl (software collections) RedHat/Centos python to build your venv, instead use system python. It is also presume that the docker is installed and properly configured on the system.



Ansible Requirements
------------

Role requires docker python modules being installed on remote system - `docker` and `docker-compose`. Check Dependancy roles section (you either can create python venv and pip install these or use existing [install-python-virtualenv](https://gitlab.com/docs-devops/community/ansible/-/tree/master/roles/runtimes/install-python-virtualenv) - the logic is adapted to use python venv on remote system. Role is idempotent - one can run it multiple times, the runner will be registered only when neeed.

Role also supplies you with shell script that is cleaning exited containers, dangling volumes and dangling images, so that you don't get into trouble with disk space utilization. Unfortunatelly gitlab-runner as docker executor still not able to do this...

Role Variables
--------------

The values of variables in defaults/main.yml are dummy ones, but this is pretty much working examples, just be sure to supply it with your own values.


#### This section more relates to pre-condition dependency role install-python-virtualenv, but should give good understanding of deps.
So, make sure these are done before you run this role actually (that includes the below pip packages being installed in your venv). 

```
python_workdir_path: "/data/python"
path_to_virtenv: "{{ python_workdir_path }}/virtenvs/docker-ci"
### python executable
python_virtenv: "{{ python_workdir_path }}/virtenvs/docker-ci/bin/python"
pip_packages:
  - pip
  - ansible
  - flake8
  - dnspython
  - Jinja2
  - docker
  - docker-compose
  - molecule[lint]
```

#### Gitlab CI section

`gitlab_url: "https://gitlab.com/"` - should be clear (can be on-premise deployment)

`remote_docker_python: "/data/python/virtenvs/docker-ci/bin/python"` - your previously deployed python venv

`docker_service_image: "python-runtime"` - type of service image runner is going to be operating as container, currently only `python-runtime` is supported

`runner_description: "docs-devops"` - runner description (will be visible in gitlab)

`runner_executor: "docker" `- type of executor

`runner_tag: "docker-runner-docs-devops"`  - should be self-explanatory

`cred_runner_registration_token: "{{ vault_cred_runner_registration_token }}"` - this variable is pretty important, that is a repository token and it sensitive (anyone having it can access your repo), hence we highly recommend to keep it encrypted. The general recommendation is to use group_vars and store token there as vaulted file. Though if you are absolutelly determined to use role's defaults/main.yml, you can. Just ansible-vault this file putting token inside (that will unfortunatelly encrypt all your variables and can posses some issues, but, hell we recommended group_vars :)).

`runner_concurrent: '5'` - this variable is used for the configuration of concurrent option for config.toml (gitlab-runner main config)

`runner_lock: False` - --locked option for gitlab registration process
Below is the list of dicts with two items (runner and service image it is going operate with) with parameters. Currently supported only list of two items, hence for each deployment you can use only one service image (if you with to have more, seperate variables in group_vars and define another pair).

```
docker_runner_images:
    - name: "gitlab-runner"
      image: "gitlab/gitlab-runner"
      alias: "docker-runner-docs-devops"
      id: "gitlab"
      tag: "latest"
    - name: "python-3.6.9-stretch"
      image: "python:3.6.9-stretch"
      alias: "python-runner-docs-devops"
      id: "python"
      tag: "3.6.9-stretch"
```

`docker_python_custom_tt_image` is a custom image built out of python-runtime (pulled from docker hub), tweaked with

`docker_python_custom_tt_image: "docs-devops-python369:latest"` 

Pip packages installed during docker build inside your custom service image.

```
docker_python_image_pip_requirements:
  - flake8
  - dnspython
  - cryptography
  - requests
  - Jinja2
  - docker
  - docker-compose
  - molecule[lint]
```


Dependency Roles
------------

[install-python-virtualenv](https://gitlab.com/docs-devops/community/ansible/-/tree/master/roles/runtimes/install-python-virtualenv) is a direct dependancy role, check README and deploy either python2 or python3 system python (not SCL). 

[docker-install](https://gitlab.com/docs-devops/community/ansible/-/tree/master/roles/containers/docker/docker-install) - make sure you check the readme and it is recommended to use this role. rather than deploy docker on your own (if you still determined, make sure the selinux support is enabaled via docker config and container-selinux package, more info [here](https://jaosorior.dev/2018/selinux-and-docker-notes/) and [here](https://success.docker.com/article/how-to-set-selinux-file-contexts-when-using-a-custom-docker-data-root))


Example Playbook
----------------

Including an example of how to use your role:

    - hosts: all
      gather_facts: True
      roles:
         - { role: ../../roles/cicd/gitlab-ci-docker-exec-deploy, tags: 'gitlab-ci-docker-exec-deploy' }


And that's how you execute it:


     ansible-playbook -i inventory/my_inventory ../../community/ansible/roles/cicd/gitlab-ci-docker-exec-deploy/gitlab-ci-docker-exec-deploy.yml


License
-------

GNU GPL license


Author Information
------------------

Docs-DevOps team @docs-devops.com
Andrii KOSTENETSKYI
Dmitry RACHKOV
