import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize('file, content', [
  ("/etc/sudoers.d/mistery_user", "Managed by Ansible"),
  ("/etc/sudoers.d/mistery_group", "Managed by Ansible")
])
def test_molecule_installed(host, file, content):
    file = host.file(file)
    assert file.exists
    assert file.contains(content)
